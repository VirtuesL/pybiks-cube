import numpy as np
import sys
from functools import reduce

class Cube(list):
  def __init__(self):
    super().__init__(self)

    self.matrix = np.full((5,5,5), None)
    self.matrix[0][1:4,1:4], self.matrix[4][1:4,1:4] = np.full((3,3),'F'), np.full((3,3), 'B')
    self.matrix[:,0][1:4,1:4], self.matrix[:,4][1:4,1:4] = np.full((3,3),'U'), np.full((3,3), 'D')
    self.matrix[:,:,0][1:4,1:4], self.matrix[:,:,4][1:4,1:4] = np.full((3,3), 'L'),  np.full((3,3),'R')

    self.faces = {'F': [(0,1),[None],[None]],
                  'B': [(4,5),[None],[None]],
                  'U': [[None],(0,1),[None]],
                  'D': [[None],(4,5),[None]],
                  'L': [[None],[None],(0,1)],
                  'R': [[None],[None],(4,5)]}
    
    self.rotatables = {'F': [(0,2),[None],[None]],
                       'B': [(3,5),[None],[None]],
                       'U': [[None],(0,2),[None]],
                       'D': [[None],(3,5),[None]],
                       'L': [[None],[None],(0,2)],
                       'R': [[None],[None],(3,5)]}

  def __getitem__(self, key):
    return self.matrix[tuple([slice(*i) for i in self.faces[key]])]
  
  def __str__(self):
    return self.matrix.__str__()

  def rotate(self, face, n):
    j = [type(i) for i in self.faces[face]].index(tuple)
    self.matrix[tuple([slice(*i) for i in self.rotatables[face]])] = np.rot90(self.matrix[tuple([slice(*i) for i in self.rotatables[face]])], -n if face == 'B' or face =='R' or face == 'U' else n, axes=(1,2) if not j else (0,2) if j == 1 else (0,1))
  
  def sequence(self, expr):
    try:
      for exp in expr.split(" "):
        face = exp[0].upper()
        n = -1 if len(exp) < 2 else 1 if exp[1] == "'" else int(exp[1])
        self.rotate(face,n)
    except Exception as e:
      print(e)

if __name__ == '__main__':
  a = Cube()
  print(a)
  try:
    while True:
      a.sequence(input("Seq "))
      print(a)
  except KeyboardInterrupt:
    exit(0)