import socket, numpy as np, sys
from functools import reduce
from cubix import Cube
from visual import gui

a = lambda x,y: x+y

class Solver:
  def __init__(self, cube):
    super().__init__()
    self.cube = cube
    self.server = socket.create_connection((socket.gethostbyname('localhost'),8080))

  def get_string(self):
    F,B = self.cube.matrix[0][1:4,1:4], np.flip(self.cube.matrix[4][1:4,1:4], 1)
    U,D = np.flip(self.cube.matrix[:,0][1:4,1:4],0), self.cube.matrix[:,4][1:4,1:4]
    L,R = np.rot90(self.cube.matrix[:,:,0][1:4,1:4],-1), np.rot90(self.cube.matrix[:,:,4][1:4,1:4][::-1],-1)
    newlist = [i for k in [U,R,F,D,L,B] for j in k for i in j]
    dfstr = reduce(a, newlist)
    self.server.sendall((dfstr+('\n')).encode())
    return self.server.recv(2048).decode()

if __name__ == "__main__":
  cube = Cube()
  solver = Solver(cube)
  g = gui(cube)
  
  if len(sys.argv) > 1:
    if sys.argv[1] == '-g' or sys.argv[1] == '--gui':
      print('Visual version')
      try:
        while True:
          g.draw()
          inp = input("Seq: ")
          dfstr = solver.get_string() if inp == 's' else cube.sequence(inp)
          if dfstr:
            dfstr = dfstr.replace('3',"'")
            dfstr = dfstr.replace('1',"")
            print(dfstr)

      except Exception as e:
        print("Error ", e)

  else:
    print("Non visual version")
    try:
      while True:
        inp = input("Seq: ")
        dfstr = solver.get_string() if inp == 's' else cube.sequence(inp)
        print(dfstr)
    except Exception as e:
      print(e)