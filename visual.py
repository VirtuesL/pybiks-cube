import matplotlib.pyplot as plt
import matplotlib.patches as pat
import matplotlib.collections as col
import numpy as np
from cubix import Cube

class gui:
  def __init__(self, cube):
    super().__init__()
    self.cube = cube
    self.fig, self.ax = plt.subplots()
    self.ax.set(xlim=(0,12),ylim=(0,9))

    self.colors = {'F':'#9c3030', 'L':'#30449c','D':'#9c9c9c','R':'#27a139','U':'#c9c92e','B':'#c97c2e'}

  def redraw(self):
    F,B = self.cube.matrix[0][1:4,1:4], np.flip(self.cube.matrix[4][1:4,1:4], 1)
    U,D = np.flip(self.cube.matrix[:,0][1:4,1:4],0), self.cube.matrix[:,4][1:4,1:4]
    L,R = np.rot90(self.cube.matrix[:,:,0][1:4,1:4],-1), np.rot90(self.cube.matrix[:,:,4][1:4,1:4][::-1],-1)
    tmp = []
    for i, row in enumerate(zip(U,L,F,R,B,D)):
      for j, val in enumerate(zip(*row)):
        tmp.append(pat.Rectangle((3+j,8-i),1,1,fc=self.colors[val[0]],ec=(0,0,0)))
        tmp.append(pat.Rectangle((0+j,5-i),1,1,fc=self.colors[val[1]],ec=(0,0,0)))
        tmp.append(pat.Rectangle((3+j,5-i),1,1,fc=self.colors[val[2]],ec=(0,0,0)))
        tmp.append(pat.Rectangle((6+j,5-i),1,1,fc=self.colors[val[3]],ec=(0,0,0)))
        tmp.append(pat.Rectangle((9+j,5-i),1,1,fc=self.colors[val[4]],ec=(0,0,0)))
        tmp.append(pat.Rectangle((3+j,2-i),1,1,fc=self.colors[val[5]],ec=(0,0,0)))
    
    self.ax.add_collection(col.PatchCollection(tmp,match_original=True))

  def draw(self):
    self.redraw()
    plt.ion()
    plt.show()
    plt.pause(0.001)


if __name__ == '__main__':
  cube = Cube()
  a = gui(cube)
  try:
    while True:
      a.draw()
      cube.sequence(input('seq '))
  except KeyboardInterrupt:
    exit(0)
